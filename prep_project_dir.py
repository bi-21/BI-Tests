from test_common.tools.project_dir import prepare_project_dir
import sys

if __name__ == '__main__':
    prepare_project_dir(sys.argv[1:][0], force_md=True, overwrite_templates=False)
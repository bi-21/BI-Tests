function deadline_tick() {
  var diff = Date.parse(deadlineDate)-Date.now();
  if (diff>0){
    var s = Math.floor((diff%(1000*60))/(1000));
    var m = Math.floor((diff%(1000*60*60))/(1000*60));
    var h = Math.floor(diff%(1000*60*60*24)/(1000*60*60));
    var d = Math.floor(diff%(1000*60*60*24*365)/(1000*60*60*24));
    document.getElementById("timeout-placeholder").innerHTML = "Still have "+Math.abs(d)+" days "+Math.abs(h)+"h "+Math.abs(m)+"min. "+s+"s before deadline.";
  }else{
    document.getElementById("timeout-placeholder").innerHTML = "Already after deadline";
  }
}
deadline_tick();
var t=setInterval(deadline_tick,1000);
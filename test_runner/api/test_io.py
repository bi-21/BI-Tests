import logging
import os
import subprocess
import sys
from pathlib import Path

import pytest
import pytest_check as check
import test_common.conf as cfg
import test_common.conf.projectdir as projectcfg
import test_common.conf.testing as testcfg
import test_common.lib.auxiliary as auxiliary
from pytest_dependency import depends
from pytest_html import extras
from test_common.conf.generic import test_runner as trgeneric
from test_common.lib.datastore.datastore import Datastore
from test_common.tools.project_dir import prepare_project_dir


@pytest.fixture(scope="module") 
def log():
    log = logging.getLogger("test_io")
    logFormatter = logging.Formatter(cfg.Common.Log.MSG_FORMAT)
    
    logdir = Path(cfg.Common.Log.FILE_PATH).parent
    if not logdir.exists():        
        os.makedirs(logdir, exist_ok=True)
    file_handler = logging.FileHandler(cfg.Common.Log.FILE_PATH.format("test_io"))
    file_handler.setFormatter(logFormatter)
    file_handler.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setFormatter(logFormatter)
    stream_handler.setLevel(cfg.Common.Log.STDOUT_LOG_LEVEL)
    log.addHandler(file_handler)
    log.addHandler(stream_handler)
    log.setLevel(logging.DEBUG)
    return log
    

# update project dir at the end of the test
@pytest.fixture(autouse=True,scope="module")
def update_project_dir():
    yield
    print("------------- cleanup --------------")
    print("--- Project dir update ---")
    prepare_project_dir(force_md=True)

@pytest.fixture(scope="module", params=trgeneric.get_test_list_params())
def datastore(request,log:logging.Logger):
    log.info(f"IOTest: setup: now testing: {request.param}")
    ds = Datastore()
    
    ds["codepath"] = request.param[2] 
    ds["taskDirPath"] = Path(ds["codepath"]).parent.absolute()
    ds["groupDirPath"] = Path(ds["codepath"]).parent.parent.absolute()
    ds["tid"] = request.param[1]
    ds["gid"] = request.param[0]
    ds["taskName"] = request.param[3] 
    
    # load testing data
    # use 'template.json' if exists in taskgroup dir
    possible_template_path:Path = ds["groupDirPath"] / f"template.json"
    if possible_template_path.exists():
        ds["testData"] = auxiliary.load_json_file(possible_template_path)['tasks'][ds['tid']]['data']
        log.info(f"testing data found at: {possible_template_path}")
    else:
        template_path:Path = testcfg.TEMPLATES_DIR / f"{ds['gid']}.json"
        ds["testData"] = auxiliary.load_json_file(template_path)['tasks'][ds['tid']]['data']
        log.info(f"testing data found at: {template_path}")

    return ds
    
@pytest.mark.dependency()
def test_compile(datastore: Datastore,extra:list, log:logging.Logger):

    passed_test_lock_path:Path = datastore["taskDirPath"] / projectcfg.Files.PASSED_LOCK_FILE_NAME
    if passed_test_lock_path.exists():
        checksum_passed = passed_test_lock_path.read_text()
        checksum = auxiliary.get_file_md5_checksum(datastore["codepath"])
        if checksum == checksum_passed:
            pytest.skip(f"IOTest: test_compile: skipping test {datastore['taskName']}, already passed.")
        else:
            os.remove(passed_test_lock_path)
            log.info(f"IOTest: test_compile: found passed.lock, but file changed, task will be tested")

    ppath = datastore["taskDirPath"] / testcfg.Compilation.PROGRAM_NAME
    os.makedirs(Path(ppath).parent.absolute(), exist_ok=True)

    cmd = [testcfg.Compilation.COMMAND] + testcfg.Compilation.ARGS + [datastore["codepath"], "-o", ppath] # TODO: refactor -> to generic conf
    cmp = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, text=cfg.Common.ENCODING)  
    
    # no timeout - projectcfg.Compilation.COMMAND is expected to be well-behaved
    out, err = cmp.communicate() 

    # append compiler output/errors to html report
    extra.extend([
        extras.text(out, name="Compiler output"),
        extras.text(err if err else "__nothing__", name="Compiler error")
    ])

    assert err == '', f"found errors when trying to compile:\n {err}"

    datastore["binaryPath"] = Path(ppath).absolute()
    log.info(f"successfully compiled: {ppath}")


@pytest.mark.parametrize('lang',trgeneric.get_test_io_lang_params())
@pytest.mark.dependency()
def test_io(datastore: Datastore, log:logging.Logger, lang:str, request):

    depends(request, [f"test_compile[{testcfg.TEST_LIST_PARAM_FORMAT.format(**datastore)}]"])
    
    log.info(f"lang set to: {lang}")
    
    strikes = 0
    passed = 0
    
    
    for data in datastore['testData'][lang]:
        p = subprocess.Popen(datastore["binaryPath"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        try:
            out, err = p.communicate(data["input"].encode(cfg.Common.ENCODING), timeout=testcfg.RUN_TIMEOUT.seconds)
            outp = out.decode(cfg.Common.ENCODING)
            if err:
                strikes+=1
                errt = f" * strike #{strikes}\n" + err.decode(cfg.Common.ENCODING)
            else:
                errt = None
        except subprocess.TimeoutExpired as e: 
            strikes+=1
            outp, errt = ("", f" * strike #{strikes}\n{e}")
            p.terminate()
    
        if errt:       
            log.error({errt})
        
        assert strikes < 3, " !! 3 strikes - skipping rest"
            
        check.is_(errt, None, f"ERROR:\n{errt}" if errt else '__nothing__')
        check.equal(outp, data["output"], f"\ninput:\n{data['input']}\ngot:\n{outp}\nbut should be:\n{data['output']}\n--------")
        if not errt and outp == data["output"]:
            passed += 1
    
    if passed == len(datastore['testData'][lang]):
        task_dir_path = Path(datastore["codepath"]).parent.absolute()
        code_checksum = auxiliary.get_file_md5_checksum(str(datastore['codepath'].absolute()))
        Path(f'{task_dir_path}\\{projectcfg.Files.PASSED_LOCK_FILE_NAME}').write_text(code_checksum)
        log.info(f"All tests on TID:{datastore['tid']} lang:{lang} passed. testlock file created.")
    
from datetime import timedelta
from pathlib import Path

TMPDIR = Path("bin\\tmp")
TEMPLATES_DIR = Path("bin\\templates")
LANGS = ["CZE", "ENG"]

TEST_LIST_PARAM_FORMAT = "{gid}/{tid}/{taskName}"

# single files must have format: */<group id>.<task id>.*
TEST_LIST = [
]

RUN_TIMEOUT = timedelta(seconds=3)

class Compilation:
    ARGS = ["-D__PROGTEST__", "-Wall", "-pedantic", "-fpermissive"]
    COMMAND = "g++"
    PROGRAM_NAME = "program.exe"

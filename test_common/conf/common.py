import logging 

ENCODING = "utf-8"
class Log:
  MSG_FORMAT = "[%(asctime)s] [%(levelname)s] %(module)s.%(funcName)s:%(lineno)d | %(message)s"
  STDOUT_LOG_LEVEL = logging.DEBUG
  FILE_PATH = "bin/logs/{0}.log"

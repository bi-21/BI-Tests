###     This file defines generically created configuration for testing api, resources, data and tests themselves
###     There is also file called 'generic.json' which is later on loaded into Webapi's Datastore 
###       and serves the same purpose as this file with the exception that it can be changed runtime
import os
from os import listdir
from os.path import isdir, isfile, join
from pathlib import Path

import pytest
import test_common.conf.driver as cfg
import test_common.conf.projectdir as projectcfg
import test_common.conf.testing as testcfg
import test_common.lib.auxiliary as auxiliary
from selenium import webdriver
from test_common.lib.datastore.datastore import Datastore


# creates specific generic data for FirefoxDriver/Webapi
class Driver:

    # returns firefox proxy setting
    def _get_firefox_capabilities_proxy():
        proxy = (cfg.DriverConfig.Firefox.Capabilities.DEFAULT_PROXY_IP,cfg.DriverConfig.Firefox.Capabilities.DEFAULT_PROXY_PORT)
        proxystr = f"{proxy[0]}:{proxy[1]}"
        return {
            "proxyType": "MANUAL",
            "httpProxy": proxystr,
            "ftpProxy": proxystr,
            "sslProxy": proxystr
        }

    def get_firefox_options():
        firefox_opt = webdriver.FirefoxOptions()
        for arg in Driver._get_geckodriver_args():
            firefox_opt.add_argument(arg)
        if cfg.DriverConfig.Firefox.BINARY_PATH:
            firefox_opt.binary = cfg.DriverConfig.Firefox.BINARY_PATH
        return firefox_opt
    
    def _get_firefox_profile_args():
        data = DatastoreHelper.recompose_datastore_from_file()
        args = Datastore.filter(self=data["driver"]["firefoxprofileargs"],names=[],white_list=False)
        for key in args:
            if type(args[key]) == str:
                args[key] = args[key].format(**os.environ)
                print(args[key])
        return args

    def get_firefox_profile():
        firefox_profile = webdriver.FirefoxProfile()
        profileargs = Driver._get_firefox_profile_args()
        for name in profileargs:
            if name == "browser.download.dir":
                arg = profileargs[name]
                if cfg.GenericMetadata.MAKE_PATH_ABSOLUTE:
                    arg = str(Path(profileargs[name]).absolute()).replace("/","\\")
                if not os.path.exists(arg):
                    os.makedirs(arg, exist_ok=True)
            else:
                arg = profileargs[name]
            firefox_profile.set_preference(name, arg)
        return firefox_profile
    
    def get_firefox_capabilities():
        firefox_capabilities = webdriver.DesiredCapabilities.FIREFOX
        firefox_capabilities['marionette'] = cfg.DriverConfig.Firefox.Capabilities.USE_MARRIONETE
        if cfg.DriverConfig.Firefox.Capabilities.USE_PROXY:
            firefox_capabilities['proxy'] = Driver._get_firefox_capabilities_proxy()
            print(f"setting proxy to:\n {firefox_capabilities['proxy']}")
        return firefox_capabilities
    
    def _get_geckodriver_args():
        json = DatastoreHelper.recompose_datastore_from_file()
        args = json["driver"]["geckodriverargs"]
        if cfg.DriverConfig.DO_RUN_HEADLESS: 
            args.append("--headless")
        return args


class DatastoreHelper:

    def recompose_datastore_from_file(filepath=cfg.GenericMetadata.FILEPATH): 
        datastore = Datastore(payload=auxiliary.load_json_file(filepath))
        def recompose_dict_to_datastore(datast):
            for key in datast:
                if type(datast[key]) is dict and "_type" in datast[key] and datast[key]["_type"] == "Datastore":
                    datast[key] = Datastore(payload=datast[key])
                    recompose_dict_to_datastore(datast[key])
        recompose_dict_to_datastore(datastore)
        return datastore

class test_runner:

    def get_test_parametrized_dependencies(test_list_params, test_name):
        dependencies = []
        for test_list_param in test_list_params:
            dependencies.append(f"{test_name}[{test_list_param}]")
        return dependencies

    def get_test_io_lang_params():
        preferred = os.getenv("TEST_LANG")
        langs = []
        for lang in testcfg.LANGS:
            if preferred != lang:
                langs.append(pytest.param(lang, marks=[
                    pytest.mark.xfail(reason=f"{preferred} lang is preferred"),
                ]))
            else:
                langs.append(pytest.param(lang))
        print(f" Each test will run for {len(langs)} lang params")
        return langs
    
    def get_test_list_params():
        for file in testcfg.TEST_LIST:
            cid = Path(file).stem.split(".")[1]
            gid = Path(file).stem.split(".")[0]
            id=Path(file).stem
            task_name = Path(file).stem
            yield pytest.param((gid,cid,file,task_name), id=testcfg.TEST_LIST_PARAM_FORMAT.format(gid=gid,tid=cid,taskName=task_name), marks=pytest.mark.dependency())
        for inf in test_runner.get_test_dir_files():
            yield pytest.param(list(inf.values()), id=testcfg.TEST_LIST_PARAM_FORMAT.format(**inf), marks=pytest.mark.dependency())
    
    # TODO: needs a fkin refactor; dont test files that already passed
    def get_test_dir_files():
        project_dir = os.getenv("TESTS_DIR")
        known_testgroups = [Path(f"{testcfg.TEMPLATES_DIR}\\{f}").stem for f in listdir(testcfg.TEMPLATES_DIR) if isfile(join(testcfg.TEMPLATES_DIR, f))]
        project_groups = auxiliary.get_dir_names(project_dir)

        available_project_groups = []
        for prep in known_testgroups:
            for dirf in project_groups:
                if dirf.startswith(prep):
                    available_project_groups.append(dirf)

        print(f"Found {len(available_project_groups)} available test groups in {project_dir}")
        
        for project_group_dir_name in available_project_groups:
            testgroup_id = project_group_dir_name.split(projectcfg.GROUP_DIR_ID_NAME_DELIMETER)[0]

            project_group_task_dirs = [ f for f in listdir(join(project_dir,project_group_dir_name)) if isdir(join(project_dir, project_group_dir_name, f))]
            prepared_testtasks = auxiliary.load_json_file(f"{testcfg.TEMPLATES_DIR}/{testgroup_id}.json")["tasks"].keys()
            
            #print(f"{project_group_task_dirs} \nx\n {prepared_testtasks}")

            available_testtasks = []
            for prep in prepared_testtasks:
                for dirf in project_group_task_dirs:
                    if dirf.startswith(prep) and os.path.exists(join(project_dir,project_group_dir_name,dirf,projectcfg.Files.PROGRAM_NAME)): 
                        available_testtasks.append(join(project_dir,project_group_dir_name,dirf,projectcfg.Files.PROGRAM_NAME))

            print(f"Found {len(available_testtasks)} available test tasks in {project_dir}/{project_group_dir_name}: {available_testtasks}")

            for task_file_path in available_testtasks:
                tid_x_taskName = Path(task_file_path).parent.name.split(projectcfg.TASK_DIR_ID_NAME_DELIMETER,1)
                yield {
                    "gid": testgroup_id,
                    "tid": tid_x_taskName[0],
                    "file": Path(task_file_path),
                    "taskName": tid_x_taskName[1]
                    }

import sqlite3
from enum import IntEnum
from typing import TYPE_CHECKING

import test_common.lib.auxiliary as utils

if TYPE_CHECKING:
    from sqlite3.dbapi2 import Cursor

    from test_common.lib.sqlitesocket import SqliteSocket, sqliteTableSocket


class SqlStatementTypes(IntEnum):
    MAIN = 1
    CONDITION = 2
    ADDITIONAL = 3
    END = 4


class SqliteQuery:
    """
    creates a sql query and executes via set SqliteSocket
    """

    EXECUTE_TIMEOUT = 15

    def __init__(self, connection:'SqliteSocket') -> None:
        self._query = ""
        self._vars = []
        self._statements = [[]]
        self._query_cursor = 0
        self._connection = connection
        self._supplements = {}

    def compose(self):
        for main_st_list in self._statements:
            main_st_list.sort(key=lambda tuple: tuple[0])
            for statement in main_st_list:
                if SqlStatementTypes.MAIN == statement[0] and not SqlStatementTypes.END == statement[0]:
                    main_st_list.append((SqlStatementTypes.END,";",()))
        proc = utils.flatten(self._statements)
        print(proc)
        types, words, vars = zip(*proc)
        print(words)
        self._vars = utils.flatten(vars)
        print(self._vars)
        self._query = " ".join(words)
        self._query = self._query.format(**self._supplements)
        print(self._query)
        self.check()

    def flush(self):
        self.compose()
        query = self._query
        self._query = ""
        self._statements = []
        return query       
    
    def execute(self) -> 'Cursor':
        self.compose()
        response = self._connection.cursor.execute(self._query, self._vars)
        return response

    def check(self) -> bool:
        temp_db = sqlite3.connect(self._connection._db_path, timeout=SqliteQuery.EXECUTE_TIMEOUT)
        try:
            temp_db.execute(self._query, self._vars)
            return True
        except Exception as e:
            print(f"Bad query: {e}")
            return False
        finally:
            temp_db.close()

    def select(self, _selector:str, _from:str, *vars):
        self._statements.append([(SqlStatementTypes.MAIN, f"SELECT {_selector} FROM {_from}", vars)])
        self._query_cursor = len(self._statements)-1
        return self
    
    def delete(self, _from:str, *vars):
        self._statements.append([(SqlStatementTypes.MAIN, f"DELETE FROM {_from}", vars)])
        self._query_cursor = len(self._statements)-1
        return self
    
    def insert(self, _into:str, **kvars ):
        names = kvars.keys()
        values = kvars.values()
        valstr = ",".join(["?"]*len(values))
        self._statements.append([(SqlStatementTypes.MAIN, f"INSERT INTO {_into} ({','.join(names)}) VALUES ({valstr})", values)])
        self._query_cursor = len(self._statements)-1
        return self
    
    def update(self, _table:str, **kvars ):
        names = kvars.keys()
        values = kvars.values()
        valstr = ", ".join([f"{name}=?" for name in names])
        self._statements.append([(SqlStatementTypes.MAIN, f"UPDATE {_table} SET {valstr}", values)])
        self._query_cursor = len(self._statements)-1
        return self

    def where(self, **kvars):
        names = kvars.keys()
        vars = kvars.values()
        valstr = ",".join([f"{name}=?" for name in names])
        self._statements[self._query_cursor].append((SqlStatementTypes.CONDITION, f"WHERE {valstr}", vars))
        return self

    def create_table(self, _name, _scheme, *vars):
        self._query_cursor = len(self._statements)-1
        supplement_name = f"{{CREATE_TABLE_SUPPLEMENT_{self._query_cursor}}}"
        
        self._statements.append([(SqlStatementTypes.MAIN, f"CREATE TABLE {supplement_name} {_name} ({_scheme})", vars)])
        self._supplements[supplement_name] = "" # set supplement fall-back
        return self

    def limit(self, _limit, *vars):
        self._statements[self._query_cursor].append((SqlStatementTypes.ADDITIONAL, f"LIMIT {_limit}",vars))
        return self

    def offset(self, _offset, *vars):
        self._statements[self._query_cursor].append((SqlStatementTypes.ADDITIONAL, f"OFFSET {_offset}", vars))
        return self
    
    def if_not_exists(self):
        self._supplements[f"CREATE_TABLE_SUPPLEMENT_{self._query_cursor}"] = "IF NOT EXISTS"
        return self
    
    def next(self):
        self._statements[self._query_cursor].append((SqlStatementTypes.END,";",()))
        return self

class TableSqliteQuery(SqliteQuery):

    def __init__(self, table_socket:'sqliteTableSocket'):
        super().__init__(table_socket)
        self.table = table_socket

    def select(self, _selector:str, *vars) -> 'TableSqliteQuery':
        return super().select(_selector, self.table.table_name, *vars)   
    
    def insert(self, **kvars) -> 'TableSqliteQuery':
        return super().insert( self.table.table_name, **kvars)

    def update(self, **kvars) -> 'TableSqliteQuery':
        return super().update(self.table.table_name, **kvars)
    
    def delete(self, *vars) -> 'TableSqliteQuery':
        return super().delete(self.table.table_name, *vars)
    
    def create_table(self, *vars):
        return super().create_table(self.table.table_name, self.table.table_scheme, *vars)

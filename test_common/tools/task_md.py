from datetime import datetime

import test_common.conf.projectdir as projectcfg
import test_common.lib.auxiliary as auxiliary
from colour import Color
from test_common.conf.projectdir import *
from test_common.lib.auxiliary import Markdown as MD
from test_common.tools.templating import TaskTemplate


class MDBanners: 
  # tags
  NO_TEST_DATA = MD.banner_block(
                    MD.color_wrap(
                        Extras.NO_TEST_DATA,
                        Colors.ORANGE,
                        Style.FONT_BOLD,
                        end=''
                    )
                  )
  
  TESTS_OK = MD.banner_block(
                    MD.color_wrap(
                        Extras.TESTS_OK,
                        Colors.NICE_GREEN,
                        Style.FONT_BOLD,
                        end=''
                    )
                  )
  
  TESTS_SKIPPED = MD.banner_block(
                    MD.color_wrap(
                        Extras.TESTS_SKIP,
                        Colors.YELLOW,
                        Style.FONT_BOLD,
                        end=''
                    )
                  ) 
  

def update_task_md(md_path, task_template:TaskTemplate, task_dir_path):
    
    now = datetime.now()
    deadline_date = datetime.strptime(task_template["deadline"], projectcfg.DATETIME_FORMAT)
    
    colours = list(Colors.NICE_RED.range_to(Colors.NICE_GREEN,11))
    # up to 200% if got more than max points
    colours.extend(Color("#44d4db").range_to(Color("#dd2cdf"),11)) 

    got_points,max_points = task_template.points
    perc = round((got_points/max_points)*100,2)
    perc_color = colours[round(perc/10)]
    
    have_test_data = True
    
    # do we have some testing data?
    if len(auxiliary.flatten(task_template.testing_sets.values())) == 0:
        task_template.markdown.header.append(MDBanners.NO_TEST_DATA)
        have_test_data = False
    else:
        task_template.markdown.header.extend([
            MD.banner_block(
                Extras.FOUND_TESTING_SETS.format(
                    setsLen=len(task_template.testing_sets),
                    sets=", ".join([f"{item}({len(task_template.testing_sets[item])})" for item in task_template.testing_sets])
                )
            ),
            Extras.NL
        ])
    
    # if passed.lock exists and no testing data found -> tests were skipped otherwise tests passed    
    if (task_dir_path / Files.PASSED_LOCK_FILE_NAME).exists():
        if have_test_data:
            task_template.markdown.header.append(MDBanners.TESTS_OK)
        else:
            task_template.markdown.header.append(MDBanners.TESTS_SKIPPED)

    # append last update datetime    
    task_template.markdown.header.extend([
        Extras.NL,
        MD.banner_block(
            Extras.DEADLINE_TIMEOUT_PLACEHOLDER
        ),
        Extras.NL,
        MD.banner_block(
            Extras.SHOW_TEMPLATE_BUTTON  
        ),
        Extras.LINE,
        Extras.LAST_UPDATE.format(lastUpdateDate=now)
    ])

    # footer: copyright :D
    task_template.markdown.footer.append(
        MD.style_wrap(
            Extras.RIGHTS.format(year=datetime.now().year),
            Style.ALING_RIGHT
        )
    )

    # create markdown payload data and create the file
    deadline_color = Colors.NICE_GREEN if now < deadline_date else Colors.NICE_RED
    format_payload = {}
    export_vars = f"var deadlineDate = \"{deadline_date}\";"
    
    format_payload.update(task_template)
    format_payload.update({
        "points": MD.color_wrap(got_points, perc_color,end=""),
        "percentPoints": MD.color_wrap(perc, perc_color,end=""),
        "deadline": MD.color_wrap(deadline_date,deadline_color,end=""),
        "headerExtra": "".join(task_template.markdown.header),
        "footer": "".join(task_template.markdown.footer),
        "deadlineDate": deadline_date,
        "scripts": "".join([
            Scripts.AS_JS.format(js=export_vars),
            Scripts.DEADLINE_TIMEOUT.format(deadlineDate=deadline_date),
            Scripts.FONTAWESOME
        ])
    })
    with open(md_path, 'w', encoding='utf-8') as f:
        f.write(Files.DEFAULT_MD.format(**format_payload))

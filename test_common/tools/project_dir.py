import os
import pickle
import shutil
from distutils.dir_util import copy_tree
from enum import Enum
from pathlib import Path

import test_common.conf.testing as tcfg
import test_common.lib.auxiliary as auxiliary
from test_common.conf.projectdir import *
from test_common.lib.auxiliary import get_files

from .task_md import update_task_md
from .templating import GroupTemplate


class TodoItemAction(Enum):
    
    # accepts kwarg 'dst' - destination where to copy
    COPY_TREE = 1
    REMOVE = 2
    MOVE = 3 
    
class TodoItem:

    def __init__(self, action: TodoItemAction, *args, **kwargs):
        self.action = action
        self.args = args
        self.kwargs = kwargs
    
    def do(self, **kwargs):
        self.kwargs.update(kwargs)
        try:
            if self.action == TodoItemAction.COPY_TREE:
                if self.kwargs["dst"] and not os.path.exists(self.kwargs["dst"]):
                    os.makedirs(self.kwargs["dst"], exist_ok=True)
                copy_tree(*self.args, **self.kwargs)
            elif self.action == TodoItemAction.REMOVE:
                os.remove(*self.args, **self.kwargs)
            elif self.action == TodoItemAction.MOVE:
                shutil.move(*self.args, **self.kwargs)
        except Exception as e:
            print(f"Project todoitem: Could not {self.action.name}:\n{e}")
    
    def __str__(self):
        return str(pickle.dumps(self))


def prepare_project_dir(path=os.getenv("TESTS_DIR"), force_md=False, overwrite_templates=False):
    
    print(f" > Preparing project dir {path}")
    
    template_ids = [Path(file).stem for file in get_files(tcfg.TEMPLATES_DIR) if Path(file).suffix == ".json"]
    
    # copy js assets to project dir
    try:
        assets_path = Path(path)/Files.PROJECT_DIR_ASSETS_DIR_NAME
        if os.path.exists(assets_path):
            shutil.rmtree(assets_path)
        shutil.copytree(Files.MARKDOWN_ASSETS_DIR_PATH,assets_path)
    except Exception as e:
        print(f"Could not copy assets. {e}")

    for template_id in template_ids:

        template_path = Path(tcfg.TEMPLATES_DIR) / f"{template_id}.json"
        group_template = GroupTemplate.from_dict(auxiliary.load_json_file(template_path))

        project_dir_path = Path(path) / f"{template_id}_{auxiliary.sanitize_str(group_template['title'])}"

        if not os.path.exists(project_dir_path):
            os.mkdir(f"{project_dir_path}")
            
        if overwrite_templates or not (project_dir_path/"template.json").exists():
            shutil.copy2(template_path, project_dir_path/"template.json")
        else:
            auxiliary.update_json_file(project_dir_path/"template.json", group_template)
        
        for task_id in group_template.tasks:
            task_dir_path = Path(project_dir_path) / f"{task_id}_{auxiliary.sanitize_str(group_template.tasks[task_id]['name'])}"
            md_path = task_dir_path / Files.MD_NAME

            if not task_dir_path.exists():
                os.mkdir(task_dir_path)

            task_program_path = task_dir_path / Files.PROGRAM_NAME

            if not task_program_path.exists():
                with open(task_program_path, 'w') as f:
                    f.write(Files.DEFAULT_PROG)
            else:
                print(f"{task_program_path} already exists")

            if not os.path.exists(md_path) or force_md:
                update_task_md(md_path, group_template.tasks[task_id], task_dir_path)
            else:
                print(f"{md_path} already exists")
            
            while len(group_template.tasks[task_id].todolist) > 0:
                todostr = group_template.tasks[task_id].todolist.pop()

                try:
                    item:TodoItem = TodoItem(*todostr)
                    item.do(dst=str(task_dir_path / "data"))
                except Exception as e:
                    print(f"Prepare project dir: could not load TodoItem: {todostr}\n{e}")

                
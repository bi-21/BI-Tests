from pathlib import Path

import test_common.lib.auxiliary as auxiliary


class Template(dict):
  pass

class GroupTemplate(Template):
  
  def __init__(self, title:str, deadline:str, id:int):
    self.update({
          "tasks": {},
          "title": title,
          "deadline": deadline,
          "id": id
        })
  
  @staticmethod
  def from_dict(dict):
    gt = GroupTemplate("","", 0)
    gt.update(dict)
    for task_id in gt.tasks:
        gt.tasks[task_id] = TaskTemplate.from_dict(gt.tasks[task_id],task_id)
    return gt
  
  @property
  def tasks(self) -> dict[str,'TaskTemplate']:
    return self["tasks"]

  def add_task_template(self, task:'TaskTemplate') -> None:
      self.tasks[task.id] = task
  
  def flush(self, dirpath, update_if_exists=False):
    path = Path(dirpath) / f"{self['id']}.json"
    if update_if_exists and path.exists():
      auxiliary.update_json_file(path, self)
    else:
      auxiliary.save_json_file(path,self) 
    return path

class TaskTemplate(Template):
  
  def __init__(self,id:int, name:str, description:str, deadline:str, max_points:int, got_points:int):
    self.id = id
    self.update({
            "description" : description,
            "maxPoints": max_points,
            "points": got_points,
            "deadline": deadline,
            "name": name,
            "todo": [],
            "pytest": PytestTemplate(),
            "md": MarkdownTemplate(),
            "data": {}
            })
  
  @staticmethod
  def from_dict(dict,id):
    tt = TaskTemplate(id,"","","", 0,0)
    tt.update(dict)
    md = MarkdownTemplate()
    md.update(tt.markdown)
    tt["md"] = md
    pytest = PytestTemplate()
    pytest.update(tt.pytest)
    tt["pytest"] = pytest
    return tt
  
  @property
  def points(self):
    return (self["points"], self["maxPoints"])
  
  @points.setter
  def _points_set(self, points):
    self["gotPoints"] = points[0]
    self["maxPoints"] = points[1]
  
  @property
  def markdown(self) -> 'MarkdownTemplate':
    return self["md"]
  
  @property
  def pytest(self) -> 'PytestTemplate':
    return self["pytest"]
  
  @property
  def todolist(self) -> list:
    return self["todo"]
  
  @property
  def testing_sets(self) -> dict:
    return self["data"]
    
class PytestTemplate(Template):
  
  def __init__(self):
    self.update({
                "testsRun": 0,
                "reportPath": "",
                "passedVersions": []
              })


class MarkdownTemplate(Template):
  
  def __init__(self):
    self.update({
                "headerExtra": [],
                "footer": []
                })   
  
  @property
  def header(self):
    return self["headerExtra"]
  
  @property
  def footer(self):
    return self["footer"]

# Usage:

## Prerequisites:

* clone repo
* install python (v3.5 or later)
* install firefox (binary should be in PATH)
* download geckodriver from https://github.com/mozilla/geckodriver/releases that supports installed firefox version 
* cd to repo:
    > pip install -r requiments

## Run test getter:
* test_getter logs-in to your progtest account, creates templates for opened tasks and downloads available testing data. If not run before tests, no test will be run because there is no template to test against. :D
### vscode:

* edit [launch.json](.vscode/launch.json) (already included in repo)
    * set PROGTEST_USR to your Progtest username
    * set PROGTEST_PASS to your Progtest password
    * set FIREFOX_DRIVER_URL to file path of geckodriver (eg. c:/bin/geckodriver.exe)
    * set TEST_DIR to your progtest project directory
* run launch setup with F5
## Project dir preparation

* project directory is automatically generated at the end of test-getter run.
* project directory will have this layout:  

    ```
        [TEST_DIR]
        │
        └───[<group id #1>_*]
        │   │
        │   └───[<task id #A>_*]
        │   │   program.c
        │   │   task.md
        │   └───[<task id #A>_*]
        │   │   └───([data])
        │   │   │   <downloads>
        │   │   └───
        │   │   program.c
        │   │   task.md
        │   └───
        │   │
        └───[<group id #2>_*]
        │   │
        │   │   
        .   .
    ```
    * where [...] means directory
    * where (...) means optional. As per description, when test-getter cannot find normalized testing data, downloaded resources from Progtest are copied to directory `data/`.
    * task dirs must include '_' after `<task id>`. 
    * group dirs must include '_' after `<group id>`. 
    * In every task dir is created file `task.md` which contains assignment description including deadline and obtained/max possible points

        * You can also use the following command to prepare/update project dir
            > python prep_project_dir.py `<project dir path>`  

        ![](doc/prep_project_dir.png)

## Run tests:

* edit [pytest.ini](pytest.ini)
    * set TEST_LANG to either 'CZE' or 'ENG'
    * set TEST_DIR to your progtest project directory

* cd to repo:
    > pytest
    * xml and html reports are created in bin/test ![](doc/pytest_html_report.png)

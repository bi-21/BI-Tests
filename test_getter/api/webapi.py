import logging
import os
import sys
import uuid
from datetime import datetime
from pathlib import Path
from time import sleep

import test_common.conf as cfg
import test_getter.api.expectedcontitions as adhocEC
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from test_common.lib.datastore.datastore import DatastoreRootProvider


class Webapi(DatastoreRootProvider):

    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.id = str(uuid.uuid4())[:8]
        super().__init__()
        self.log = logging.getLogger(cfg.Driver.WebapiConfig.LOGGER_NAME)
        logFormatter = logging.Formatter(cfg.Common.Log.MSG_FORMAT)
        
        logdir = Path(cfg.Common.Log.FILE_PATH).parent
        if not logdir.exists():        
            os.makedirs(logdir, exist_ok=True)
        file_handler = logging.FileHandler(
            cfg.Common.Log.FILE_PATH.format(cfg.Driver.WebapiConfig.LOGGER_NAME),'w', cfg.Common.ENCODING)
        file_handler.setFormatter(logFormatter)
        file_handler.setLevel(logging.DEBUG)
        stream_handler = logging.StreamHandler(sys.stdout)
        stream_handler.setFormatter(logFormatter)
        stream_handler.setLevel(cfg.Common.Log.STDOUT_LOG_LEVEL)
        self.log.addHandler(file_handler)
        self.log.addHandler(stream_handler)
        self.log.setLevel(logging.DEBUG)
    
    def sync_static_log_format(self):
        logging.basicConfig(level=logging.INFO,format=cfg.Common.Log.MSG_FORMAT)
    
    def _awaitUntil(self, condition, exp_wait=cfg.Driver.DriverConfig.DEFAULT_WAIT):
        try:
            WebDriverWait(self.driver, exp_wait).until(condition)
            return True
        except TimeoutException:
            self.log.info(f"Timeout! Condition {condition} not met")
            return False

    def _awaitUntilNot(self, condition, exp_wait=cfg.Driver.DriverConfig.DEFAULT_WAIT):
        try:
            Element = WebDriverWait(self.driver, exp_wait).until_not(condition)
            return True
        except TimeoutException:
            self.log.info(f"timeout!")
            return False

    def implicitly_wait(self, time=cfg.Driver.DriverConfig.IMPLICIT_WAIT):
        self.driver.implicitly_wait(time)

    def go_to_url(self, url):
        self.driver.get(url)
        self.implicitly_wait()

    def hover(self, locator):
        if cfg.Driver.WebapiConfig.DO_HOVER:
            element = self.find(locator)
            hov = ActionChains(self.driver).move_to_element(element)
            hov.perform()
    
    def double_click(self, locator):
        self.wait_until_visible(locator)
        self.await_until_clickable(locator)
        element = self.find(locator)
        dclick = ActionChains(self.driver).double_click(element)
        dclick.perform()

    def repeated_click(self, locator, repetitions = 2):
        self.wait_until_visible(locator)
        self.await_until_clickable(locator)
        for _ in range(repetitions):
            self.driver.find_element(*locator).click()

    def hover_and_click(self, locator):
        self.hover(locator)
        self.wait_until_visible(locator)
        self.await_until_clickable(locator)
        self.driver.find_element(*locator).click()

    def safe_click(self, locator):
        self.wait_until_visible(locator)
        self.await_until_clickable(locator)
        self.driver.find_element(*locator).click()
        self.log.info(f"clicked on {locator}")

    def hover_and_click_first(self, locator):
        elements = self.driver.find_element(*locator)
        hov = ActionChains(self.driver).move_to_element(elements[0])
        if cfg.Driver.WebapiConfig.DO_HOVER:
            hov.perform()
        elements[0].click()
        self.log.info(f"clicked on {locator}")


    def is_visible(self, locator, exp_wait=cfg.Driver.DriverConfig.DEFAULT_WAIT):
        try:
            visible = self.wait_until_visible(locator, exp_wait=exp_wait)
            if visible:
                # self.log.info(f'found. visible = "{visible}"')
                return True
            else:
                self.log.warning(f'locator {locator} still not visible after {exp_wait} seconds')
                return False
        except:
            self.log.error(f'locator {locator} not found')
            return False

    def scroll_into_view(self, locator):
        self.implicitly_wait()
        if not self.wait_until_present(locator):
            self.log.warning(f"locator {locator} still not present")
            return
        elem = self.driver.find_element(*locator)
        if cfg.Driver.WebapiConfig.DO_SCROLL_TO:
            self.driver.execute_script('arguments[0].scroll_into_view({behavior: "smooth", block: "center", inline: "center"});', elem)
        if not self.wait_until_visible(locator):
            self.log.warning(f"element {locator} still not visible ")
        if not self.await_until_clickable(locator):
            self.log.warning(f"scrolled, but element is not clickable")
        self.log.info(f"scrolled to {locator}")

    def scroll_first_into_view(self, locator):
        self.implicitly_wait()
        self.wait_until_present(locator)
        elem = self.find_all(locator)
        if cfg.Driver.WebapiConfig.DO_SCROLL_TO:
            self.driver.execute_script('arguments[0].scroll_into_view({behavior: "smooth", block: "center", inline: "center"});', elem[0])
        self.await_until_clickable(locator)

    def click_on_safe(self, locator):
        self.scroll_into_view(locator)
        self.safe_click(locator)

    def scroll_n_click(self, locator):
        self.scroll_into_view(locator)
        self.safe_click(locator)

    def click_on_first_safe(self, locator):
        self.scroll_first_into_view(locator)
        self.hover_and_click_first(locator)

    def send_keys_safe(self, locator, keys, do_clear=True, send_enter=True):
        self.scroll_into_view(locator)
        element = self.find(locator)
        if not element.is_displayed():
            self.implicitly_wait()
        if do_clear:
            element.clear()
        element.send_keys(keys)
        self.implicitly_wait()
        if send_enter:
            element.send_keys(Keys.ENTER)
        self.log.info(f"sent keys '{keys}' to {locator}")

    def find(self, locator):
        return self.driver.find_element(*locator)

    def find_all(self, locator):
        return self.driver.find_elements(*locator)

    def await_downloaded(self, down_dir=None, refresh_time=0.2, print_rate = 1, max_download_age=cfg.Driver.DriverConfig.Download.MAX_FILE_CHANGE_AGE):
        if down_dir == None:
            down_dir = self.datastore["_preferences"]["profile"].default_preferences["browser.download.dir"]
            self.log.info(f"down_dir: {down_dir}")
        timer = avg_speed = last_speed = no_speed_counter = fsize = last_size = 0
        is_downloading = True
        filepath = None
        print_rate = refresh_time * print_rate
        
        self.log.info(f"Looking for downloading file in {down_dir}")
        while is_downloading:
            sleep(refresh_time)
            if timer > cfg.Driver.DriverConfig.Download.DOWNLOADING_TIMEOUT:
                raise Exception(f"Download exceeded {cfg.Driver.DriverConfig.Download.DOWNLOADING_TIMEOUT}S")
            if filepath == None or not os.path.exists(filepath):
                filelist = [down_dir + "/" +
                            f for f in os.listdir(down_dir)]
                if len(filelist) > 0:
                    filepath = max(filelist, key=os.path.getctime)
                    if(datetime.fromtimestamp(os.path.getmtime(filepath)) - datetime.now() > max_download_age):
                        filepath = None
                        continue
                else:
                    continue
            fsize = os.path.getsize(filepath)/(8*1024)
            avg_speed = (avg_speed * timer + (fsize - last_size)) / (timer + refresh_time)
            timer += refresh_time
            print(f"downloading {timer}s, size: {fsize:.3f}KB, avg: {avg_speed:.3f}KB", end="\r")
            if timer % print_rate == 0:
                print(f"downloading {timer}s, size: {fsize:.3f}KB, avg: {avg_speed:.3f}KB", end="\r")
            if fsize == last_size:
                no_speed_counter += refresh_time
            else:
                no_speed_counter = 0
                last_size = fsize
            if not filepath.endswith("download"):
                is_downloading = False
                self.log.info(f"downloaded '{filepath}' in {timer}s, size: {fsize:.3f}KB")
                break
            if no_speed_counter > cfg.Driver.DriverConfig.Download.NO_SPEED_TIMEOUT:
                os.remove(filepath)
                raise Exception(
                    "Seems not to be downloading; skipping and deleting file")
        return (filepath, fsize)

    def wait_until_visible(self, locator, exp_wait=cfg.Driver.DriverConfig.DEFAULT_WAIT):
        return self._awaitUntil(EC.visibility_of_element_located(locator), exp_wait=exp_wait)

    def wait_until_not_visible(self, locator):
        return self._awaitUntilNot(EC.visibility_of_element_located(locator))

    def wait_until_present(self, locator):
        return self._awaitUntil(EC.presence_of_element_located(locator))

    def await_until_not_found(self, locator):
        return self._awaitUntilNot(adhocEC.can_find(locator))

    def await_until_not_found_any(self, locator):
        return self._awaitUntilNot(adhocEC.can_find_any(locator))

    def await_until_clickable(self, locator):
        return self._awaitUntil(EC.element_to_be_clickable(locator))

    def await_element_visible(self, locator):
        return self._awaitUntil(EC.visibility_of_all_elements_located(locator))

    def await_element_property_value_to_be(self, locator, property_name, property_value):
        return self._awaitUntil(adhocEC.property_value_to_be(locator, property_name, property_value))

    def await_element_property_value_to_contain(self, locator, property_name, property_value, exp_wait=cfg.Driver.DriverConfig.DEFAULT_WAIT):
        return self._awaitUntil(adhocEC.property_value_to_contain(locator, property_name, property_value), exp_wait=exp_wait)

    def await_element_property_value_not_to_contain(self, locator, property_name, property_value):
        return self._awaitUntilNot(adhocEC.property_value_to_contain(locator, property_name, property_value))

    def await_element_comply_to_lambda_condition(self, locator, condition):
        return self._awaitUntil(adhocEC.lambda_condition(locator, condition))

    def is_displayed(self, locator):
        try:
            element = self.driver.find_element(*locator)
            if element.is_displayed():
                return True
            else:
                self.log.info('element found, but not displayed')
                return False
        except:
            self.log.info('element not found')
            return False

    def close_browser(self):
        self.driver.close()
    
    def await_url(self, page_url):
        self._awaitUntil(EC.url_to_be(page_url))
        
    @staticmethod    
    def build() -> 'Webapi':
        ## set up firefox driver
        options = cfg.Generic.Driver.get_firefox_options()
        profile = cfg.Generic.Driver.get_firefox_profile()
        capabilities = cfg.Generic.Driver.get_firefox_capabilities()
        driverpath = cfg.Driver.DriverConfig.Firefox.DRIVER_PATH
        binpath = cfg.Driver.DriverConfig.Firefox.BINARY_PATH

        driver = webdriver.Firefox(options=options, firefox_profile=profile, capabilities=capabilities, executable_path=driverpath, firefox_binary=binpath)

        if cfg.Driver.BROWSER_MAXIMALIZE:
            driver.maximize_window()
        else:
            driver.set_window_size(*cfg.Driver.DriverConfig.WIN_SIZE)
        
        ## ---------------------

        webapi = FirefoxWebapi(driver)
        webapi.log.info(f"Webapi setup:\n executable in {driverpath}\n profile: {profile}\n options: {options}\n capabalities: {capabilities}")
        webapi.log.info("Updating webapi datastore")
        webapi.datastore["_preferences"].update({
            "options": options,
            "profile": profile,
            "capabilities": capabilities
        })
        webapi.datastore.update(cfg.Generic.DatastoreHelper.recompose_datastore_from_file())
        return webapi

class FirefoxWebapi(Webapi):

    def __init__(self, driver: WebDriver):
        super().__init__(driver)




    
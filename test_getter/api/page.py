from datetime import datetime

from test_common.lib.datastore.datastore import DatastoreSubscriber
from test_getter.api.webapi import FirefoxWebapi


# defines basic POM element component, each instance has unique uuid
class POMElement(DatastoreSubscriber):

    def __init__(self, webapi: FirefoxWebapi):
        super().__init__()
        self.webapi = webapi
        self.subscribe(provider=webapi)
        self.webapi.log.debug(f"created {self.datastore['_subtype']}[id:{self.id}] in {self.parent_datastore['_type']}[{'id:'+self.parent_datastore.id if hasattr(self.parent_datastore, 'id') else '-'}] at {self.created}")
        
    @property
    def driver(self):
        return self.webapi.driver
    
    @property
    def created(self) -> datetime:
        return datetime.fromtimestamp(self.datastore["_created"])

# defines POM element in role of web page with specific url
class Page(POMElement):

    def __init__(self, webapi: FirefoxWebapi, page_url):
        super().__init__(webapi)
        self.url = page_url
        self.datastore["url"] = self.url
    
    def open(self) -> None:
        self.webapi.log.info(f"url: {self.url}")
        self.driver.get(self.url)

    def check_right_url(self) -> bool:
        return self.driver.current_url == self.url

    def await_page_loaded(self) -> None:
        self.webapi.await_url(self.url)
    
    def load(self) -> bool:
        self.open()
        self.await_page_loaded()
        return self.check_right_url()

# defines POM element in role of standalone document inside parent POM element
class Container(POMElement):

    def __init__(self, parent: POMElement, app_locator: tuple):
        self.app_locator = app_locator
        self.parent = parent
        super().__init__(parent.webapi)
        self.subscribe(parent)
    
    def await_visible(self):
        self.webapi.await_element_visible(self.app_locator)
    
    def find_inside(self, locator):
        return self.webapi.find(self.app_locator).find_element(*locator)

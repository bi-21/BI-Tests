import glob
import os
import shutil
from datetime import datetime
from os import listdir
from os.path import isfile, join
from pathlib import Path
from typing import TYPE_CHECKING

import test_common.conf.projectdir as projectcfg
import test_common.conf.testing as testcfg
import test_common.lib.auxiliary as auxiliary
from test_common.tools.project_dir import TodoItemAction
from test_common.tools.templating import TaskTemplate
from test_getter.api.page import Page
from test_getter.resources.locators import TaskPageLocators as locators

if TYPE_CHECKING:
    from test_common.tools.templating import GroupTemplate
    from test_getter.resources.pom.taskgrp_page import ProgtestTaskGroupPage


class ProgtestTaskPage(Page):

    def __init__(self, group_page: 'ProgtestTaskGroupPage', url):
        super().__init__(group_page.webapi, url)
        self.group_page = group_page

    @property
    def template(self) -> 'TaskTemplate':
        return self.group_page.template.tasks[self.id]

    def _get_tmp_task_path(id) -> Path:
        return Path(testcfg.TMPDIR) / auxiliary.sanitize_str(id)

    def update_task_template(self):
        name = self.webapi.find(locators.TITLE).text
        desc = self.webapi.find(
            locators.DESCRIPTION).get_attribute('innerHTML')
        maxpoints = float(self.webapi.find(locators.MAX_POINTS).text)
        gotpoints = float(self.webapi.find(locators.POINTS).text)
        textdate = self.webapi.find(locators.DEADLINE_DATE).text
        date = datetime.strptime(textdate, "%d.%m.%Y %H:%M:%S")
        datestr = date.strftime(projectcfg.DATETIME_FORMAT)

        self.id = auxiliary.get_url_param(self.url, "Tsk")

        self.group_page.template.add_task_template(
            TaskTemplate(self.id, name, desc, datestr, maxpoints, gotpoints))

    def download_testing_data(self):
        self.webapi.click_on_safe(locators.TESTING_DATA)
        path, size = self.webapi.await_downloaded()

        auxiliary.extract_tgz(path, ProgtestTaskPage._get_tmp_task_path(self.id))
        os.remove(path)

    # TODO: refactor -> logic should not be in pom
    def update_testing_data_to_template(self):
        testingdata = {}
        do_remove_tmp_dir = True
        sets = []
        for lang in testcfg.LANGS:
            testingdata[lang] = {}
            sets.extend(
                glob.glob(f'{ProgtestTaskPage._get_tmp_task_path(self.id)}/**/{lang}', recursive=True))

        self.webapi.log.info(f"TaskPage: Found possible testing data sets: {sets}")

        for set in sets:
            lang = Path(set).stem
            if os.path.exists(set):
                files = [join(set, f)
                         for f in listdir(set) if isfile(join(set, f))]
                data = {}

                for file in files:
                    path = Path(file)
                    text = path.read_text()
                    index = int(path.stem.split("_")[0])
                    if not index in data:
                        data[index] = {}
                    if path.stem.endswith("_out_win"):
                        data[index]["output_win"] = text
                    elif path.stem.endswith("_out"):
                        data[index]["output"] = text
                    elif path.stem.endswith("_in"):
                        data[index]["input"] = text

                testingdata[lang] = list(data.values())

        if len(sets) == 0:
            do_remove_tmp_dir = False
            self.webapi.log.warning(
                f"   !! cannot find normalized IO testing data, please test manually :{{")
            self.template.todolist.append(
                (TodoItemAction.COPY_TREE, str(ProgtestTaskPage._get_tmp_task_path(self.id)))
            )
            self.template.markdown.header.extend([
                projectcfg.Extras.NORMALIZED_TESTING_DATA_NOT_FOUND,
                projectcfg.Extras.NL
            ])
            testingdata[lang] = {}

        self.template.testing_sets.update(testingdata)

        if do_remove_tmp_dir:
            try:
                shutil.rmtree(ProgtestTaskPage._get_tmp_task_path(self.id))
            except Exception as e:
                self.webapi.log.error(
                    f"could not remove tmp test data: task:{self.id} error: {e}")
        else:
            self.webapi.log.info(f"Tmp data will be preserved for taskID:{self.id}")
        self.webapi.log.info(
            f"updated testing data for grpid:{self.group_page.datastore['template']['id']} taskid:{self.id}")

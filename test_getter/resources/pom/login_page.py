from test_getter.api.page import Page
from test_getter.api.webapi import FirefoxWebapi
from test_getter.resources.locators import CommonLocators as clocators
from test_getter.resources.locators import \
    ProgtestLoginPageLocators as locators


class ProgtestLoginPage(Page):

    def __init__(self, webapi: FirefoxWebapi):
        super().__init__(webapi, clocators.PROGTEST_URL)

    def login(self, username:str, password:str):
        self.webapi.click_on_safe(locators.DROPDOWN)
        self.webapi.click_on_safe(locators.DROPDOWN_OPTION)
        self.webapi.click_on_safe(locators.USERNAME_INPUT)
        self.webapi.send_keys_safe(locators.USERNAME_INPUT, username,send_enter=False)
        self.webapi.click_on_safe(locators.PASS_INPUT)
        self.webapi.send_keys_safe(locators.PASS_INPUT, password,send_enter=False)
        self.webapi.find(locators.LOGIN_BUTTON).submit()
    
        

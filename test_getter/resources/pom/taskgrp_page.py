from datetime import datetime

import test_common.lib.auxiliary as auxiliary
from test_common.tools.templating import GroupTemplate
from test_getter.api.page import Page
from test_getter.api.webapi import FirefoxWebapi
from test_getter.resources.locators import TaskGroupPageLocators as locators

from .task_page import ProgtestTaskPage


class ProgtestTaskGroupPage(Page):

    def __init__(self, webapi:FirefoxWebapi, url):
        super().__init__(webapi, url)
        
    @property
    def template(self) -> 'GroupTemplate':
        return self.datastore["template"]
    
    @property
    def tasks_count(self):
        return len(self.datastore["tasks"])
    
    
    def make_taskgroup_template(self):
        title =  self.webapi.find(locators.TITLE).text
        textdate = self.webapi.find(locators.DEADLINE_DATE).text
        date = str(datetime.strptime(textdate,"%d.%m.%Y %H:%M:%S"))
        id = auxiliary.get_url_param(self.url, "Tgr")
        self.datastore["template"] = GroupTemplate(title, date, id)
    
    def flush_template(self, dirpath:str, overwrite=False):
        return self.template.flush(dirpath, overwrite)
    
    def find_tasks(self):
        buttons = self.webapi.find_all(locators.TASK_DETAIL)
        hrefs = [butt.get_attribute("href") for butt in buttons]
        self.datastore["tasks"] = hrefs
        self.webapi.log.info(f"found {len(self.datastore['tasks'])} open tasks in BI-PA:{self.datastore['template']['title']}")
    
    def pop_task(self):
        taskhref = self.datastore['tasks'].pop()
        return ProgtestTaskPage(self,taskhref)

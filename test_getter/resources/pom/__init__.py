__ALL__ = ["bipa_page", "login_page", "task_page", "taskgrp_page"]

from .bipa_page import ProgtestBIPAPage
from .login_page import ProgtestLoginPage
from .task_page import ProgtestTaskPage
from .taskgrp_page import ProgtestTaskGroupPage
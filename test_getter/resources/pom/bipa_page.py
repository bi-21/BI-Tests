import test_common.lib.auxiliary as auxiliary
from test_getter.api.page import Page
from test_getter.api.webapi import FirefoxWebapi
from test_getter.resources.locators import ProgtestMainPageLocators as locators

from .taskgrp_page import ProgtestTaskGroupPage


class ProgtestBIPAPage(Page):

    def __init__(self, webapi: FirefoxWebapi):
        super().__init__(webapi, locators.URL)
    
    
    def find_taskgroups(self):
        self.webapi.click_on_safe(locators.BI_PA_BUTTON)
        buttons = self.webapi.find_all(locators.SHOW_BUTTON)
        hrefs = [butt.get_attribute("href") for butt in buttons]
        self.datastore["taskgroups"] = [href for href in hrefs if not href.startswith('javascript:') and auxiliary.get_url_param(href,"X") == "TaskGrp"]
        self.webapi.log.info(f"found {len(self.datastore['taskgroups'])} open taskgroups in BI-PA")
    
    def pop_taskgroup(self):
        href = self.datastore['taskgroups'].pop()
        return ProgtestTaskGroupPage(self.webapi, href)
    
    @property
    def groups_count(self):
        return len(self.datastore['taskgroups'])

from selenium.webdriver.common.by import By

class CommonLocators:

    PROGTEST_URL = 'https://progtest.fit.cvut.cz/index.php'
    BODY = (By.TAG_NAME, "body")

class ProgtestLoginPageLocators:
    
    # click 'select'
    DROPDOWN = (By.CSS_SELECTOR, 'select[name="UID_UNIVERSITY"]')
    # click 'select > option[value="2"]'
    DROPDOWN_OPTION = (By.CSS_SELECTOR, 'select[name="UID_UNIVERSITY"] > option[value="2"]')
    # fill 'input[name="USERNAME"]' | project var 'PROGTEST_USR' 
    USERNAME_INPUT = (By.CSS_SELECTOR, 'input[name="USERNAME"]')
    # fill 'input[name="PASSWORD"]' | project var 'PROGTEST_PASS'
    PASS_INPUT = (By.CSS_SELECTOR, 'input[name="PASSWORD"]') 
    # click 'input[name="Login"]' 
    LOGIN_BUTTON = (By.CSS_SELECTOR, '#ldap3 > td > table > tbody > tr > td:nth-child(1) > div > input')

class ProgtestMainPageLocators:

    URL = f"{CommonLocators.PROGTEST_URL}?X=Main"
    # click 
    BI_PA_BUTTON = (By.XPATH, "//a[contains(text(),'BI-PA1')]")
    # click through open pages
    SHOW_BUTTON = (By.XPATH, "//a[contains(text(),'Zobrazit')]")

class TaskGroupPageLocators:

    # store text to template
    TITLE = (By.CSS_SELECTOR, "body > center:nth-child(5) > div > div > table > tbody > tr > td > b")
    # store text to template
    DEADLINE_DATE = (By.CSS_SELECTOR, "#maintable > tbody > tr:nth-child(1) > td.tCell > b")

    # click through open tasks
    TASK_DETAIL = (By.XPATH, "//a[contains(text(),'Detaily')]")

class TaskPageLocators:

    # store text to template
    TITLE = (By.CSS_SELECTOR, "body > center > div > div > table > tbody > tr > td > b")
    # store text to template
    DEADLINE_DATE = (By.CSS_SELECTOR, "#maintable > tbody > tr:nth-child(1) > td.tCell > b")
    # store text to template
    MAX_POINTS = (By.XPATH, "//*[contains(text(),'Max. hodnocení:')]/parent::td/parent::tr/td[2]/b")
    # store text to template
    POINTS = (By.XPATH, "//*[contains(text(),'Hodnocení:')]/parent::td/parent::tr/td[2]/b")
    # store innerHTML to template
    DESCRIPTION = (By.CSS_SELECTOR, "#maintable td.lrtbCell")

    # download | use pt_to_ut tool to obtain json testing data
    TESTING_DATA = (By.XPATH, "//a[contains(text(),'Download')]")

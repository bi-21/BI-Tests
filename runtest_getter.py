# -*- coding: utf-8 -*-
import os

import test_common.conf.testing as testcfg
from test_common.tools import prepare_project_dir
from test_getter.api.webapi import Webapi
from test_getter.resources.pom import ProgtestBIPAPage, ProgtestLoginPage

if __name__ == '__main__':
    api = Webapi.build()
    api.sync_static_log_format()

    loginPage = ProgtestLoginPage(api)
    bipaPage = ProgtestBIPAPage(api)

    loginPage.load()
    loginPage.login(os.getenv('PROGTEST_USR'), os.getenv('PROGTEST_PASS'))

    bipaPage.await_page_loaded()
    bipaPage.find_taskgroups()

    while bipaPage.groups_count > 0:
        grp = bipaPage.pop_taskgroup()
        grp.load()
        grp.make_taskgroup_template()
        grp.find_tasks()
        while grp.tasks_count > 0:
            task = grp.pop_task()
            task.load()
            task.update_task_template()
            task.download_testing_data()
            task.update_testing_data_to_template()
        templatepath = grp.flush_template(testcfg.TEMPLATES_DIR)
        api.log.info(f"New testing testing template: {templatepath}")
        
    prepare_project_dir(force_md=True)
    